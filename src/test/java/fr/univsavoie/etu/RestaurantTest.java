package fr.univsavoie.etu;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;





public class RestaurantTest {

	@Test
	public void aNewTicketShouldBeEnAttente() {
		Ticket t = new Ticket(null);
		
		Assert.assertTrue(t.etat instanceof EtatTicketEnAttente);
	}
	
	
	@Test
	public void aNewOrderShouldBeEnAttente() {
		Commande c = new Commande();
		
		assertEquals(c.etat, EtatCommande.EnAttente);
	}

}
