package fr.univsavoie.etu;

public class EtatTicketEnAttente implements EtatTicket {

	public final Ticket ticket;
	public EtatTicketEnAttente(Ticket t) {
		// TODO Auto-generated constructor stub
		this.ticket = t;
		
	}	
	@Override
	public void aEnvoyer() {
		// TODO Auto-generated method stub
		ticket.setEtatTicket(new EtatTicketEstEnvoye(ticket));
		ticket.notifierObservateurs();
		ticket.notifyObservers();

	}

	@Override
	public void aPreparer() {
		// TODO Auto-generated method stub

	}

	@Override
	public void aVenirChercher() {
		// TODO Auto-generated method stub

	}

	@Override
	public void aRecuperer() {
		// TODO Auto-generated method stub

	}
	@Override
	public void notifierObservateurs() {
		// TODO Auto-generated method stub
		//ticket.setChanged();// M�thode de l'API.
        ticket.notifyObservers();// Egalement une m�thode de l'API.
		
	}
	@Override
	public boolean estBar() {
		for (Plat p : ticket.listePlats) {
			if (p instanceof Boisson) {
			}
			else {
				return false;
			}
		}
		return true;
		
	}
	@Override
	public boolean estCuisine() {
		for (Plat p : ticket.listePlats) {
			if (p instanceof Boisson) {
				return false;
			}
		}
		return true;
		
	}
	
	public String toString(){
		return " En Attente";
	}

}
