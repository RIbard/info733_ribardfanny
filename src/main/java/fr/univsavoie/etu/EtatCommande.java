package fr.univsavoie.etu;

public enum EtatCommande {
	EnAttente,EnCreation,Prete,EnPreparation,EnAttentePayement,Reglee;

	public String toString(){
		switch (this) {
		case EnAttente:
			return "En attente";
		case EnCreation:
			return "En creation";
		case Prete:
			return "Prete";
		case EnPreparation:
			return "En preparation";
		case EnAttentePayement:
			return "En attente payement";
		case Reglee:
			return "Reglee";
		default:
			break;
		}
		return null;
	}
}


